
function checkFirstName(){
	var firstName = document.getElementById("firstName").value;
	var paragraph = document.getElementById("firstname1");
	var message='';
if(firstName.length < 2 || firstName.length > 40)
	message = "Please insert between 2 and 40 characters";
if (firstName == null || firstName == ""){
	message = "First name is mandatory!";
}
	displayValues(paragraph, message);
}


function checkLastName(){
	var lastName = document.getElementById("lastName").value;
	var paragraph = document.getElementById("lastname1");
	var message='';	
if(lastName.length < 2 || lastName.length > 40)
	message = "Please insert between 2 and 40 characters";
if (lastName == null || lastName == ""){
	message = "Last name is mandatory!";
}
	displayValues(paragraph,message);
}


function checkUsername(){
	var userName = document.getElementById("username").value;
	var paragraph = document.getElementById("username1");
	var message='';	
if(username.length < 2 || userName.length > 40)
	message = "Please insert between 2 and 40 characters";
if (userName == null || userName == ""){
	message = "Username is mandatory!";
}
	displayValues(paragraph,message);
}


function checkEmail(){
	var eMail = document.getElementById("email").value;
	var paragraph = document.getElementById("email1");
	var message='';	
if(eMail.length < 2 || eMail.length > 40)
	message = "Please insert between 2 and 40 characters";
if (eMail == null || eMail == ""){
	message = "Email is mandatory!";
}
	displayValues(paragraph,message);
}


function checkPassword(){
	var password = document.getElementById("password").value;
	var paragraph = document.getElementById("password1");
	var message='';	
if(password.length < 2 || password.length > 40)
	message = "Please insert between 2 and 40 characters";
if (password == null || password == ""){
	message = "Password is mandatory!";
}
	displayValues(paragraph,message);
}


function checkConfirmPassword(){
	var confirmpassword = document.getElementById("confirmpassword").value;
	var paragraph = document.getElementById("confirmpassword1");
	var message='';	
if(confirmpassword.length < 2 || confirmpassword.length > 40)
	message = "Please insert between 2 and 40 characters";
if (confirmpassword == null || confirmpassword == ""){
	message = "Confirm password field is mandatory!";
}
	displayValues(paragraph,message);
}




function checkAbout(){
	var aboutYourself = document.getElementById("about").value;
	var paragraph = document.getElementById("about1");
	var message='';	
if(aboutYourself.length < 2 || aboutYourself.length > 40)
	message = "Please insert between 2 and 40 characters!";
if (aboutYourself == null || aboutYourself == ""){
	message = "It's mandatory to insert a description about yourself!";
}
	displayValues(paragraph,message);
}



function verifyCheckbox(){
	var checkBox = document.getElementById("checkbox");
	var paragraph = document.getElementById("checkbox1");
	var message = '';
	if (checkBox.checked == false){
		message = "Checkbox needs to be checked!";
	}
	displayValues(paragraph,message);
}


function checkRadio(){
	var checkMale = document.getElementById("male");
	var checkFemale = document.getElementById("female");
	var paragraph = document.getElementById("radio1");
	var message = '';
	if (checkMale.checked == false && checkFemale.checked == false){
		message = "Please select a gender!";
	}
	displayValues(paragraph,message);
}



function displayValues(element, message){
	element.innerHTML=message;
}


function validateNotEmpty(element){
	if (element=='' || element==null){
		return true;
	} else 
		return false;
}



function checkAll(){
	checkFirstName();
	checkLastName();
	checkEmail();
	checkPassword();
	checkConfirmPassword();
	checkUsername();
	checkAbout();
	checkRadio();
	verifyCheckbox();
}




function submit(){
	var firstName = document.getElementById("firstName").value;
	var lastName = document.getElementById("lastName").value;
	var userName = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	var confirmpassword = document.getElementById("confirmpassword").value;
	var aboutYourself = document.getElementById("about").value;
	var checkMale = document.getElementById("male");
	var checkFemale = document.getElementById("female");
	var aboutYourself = document.getElementById("about").value;
	var checkBox = document.getElementById("checkbox");
	var paragraph = document.getElementById("regIsCompleted");
	var message = '';
	
if(firstName.length > 2 && firstName.length < 40){
	if(lastName.length > 2 && lastName.length < 40){
		if(userName.length > 2 && userName.length < 40){
			if(confirmpassword.length > 2 && confirmpassword.length < 40){
				if(password.length > 2 && password.length < 40){
					if(aboutYourself.length > 2 && aboutYourself.length <40){
						if (checkMale.checked == true || checkFemale.checked == true){
							if (checkBox.checked == true){
								message = "User registered succesfully.";
						}
					}
				}
			}
		}
	}
}}
displayValues(paragraph,message);
}
	